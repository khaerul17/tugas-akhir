FROM node:16-alpine AS deps
RUN apk add --no-cache libc6-compat
RUN apk add --update --no-cache curl py-pip
RUN apk add --no-cache make python3 g++ gcc libgcc libstdc++
RUN npm install --quiet node-gyp -g
WORKDIR /app
COPY package.json ./
RUN yarn install  --frozen-lockfile

FROM node:16-alpine AS builder

RUN yarn cache clean && yarn install --frozen-lockfile 

# Rebuild the source code only when needed
FROM node:16-alpine AS builder

WORKDIR /app
RUN export NODE_ENV="production"
ENV NODE_ENV production 
RUN export NODE_OPTIONS=\"--max_old_space_size=4096\" 

COPY . .
COPY --from=deps /app/node_modules ./node_modules
RUN yarn build && yarn install --production --ignore-scripts --prefer-offline

FROM node:16-alpine AS runner
WORKDIR /app
ENV NODE_ENV production
RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001
# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder --chown=nextjs:nodejs /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/src ./src
COPY --from=builder /app/webpack-extends ./webpack-extends
COPY --from=builder /app/.env ./.env
#COPY --from=builder /app/.babelrc ./.babelrc
COPY --from=builder /app/next-env.d.ts ./next-env.d.ts
COPY --from=builder /app/next.config.js ./next.config.js

USER nextjs

EXPOSE 3000
